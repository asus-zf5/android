CyanogenMod 13.0 (Zenfone 5)
============================

This is the official project of CyanogenMod 13.0 for Zenfone 5.


Submitting Issues
-----------------

To submit an issue, please go to [Create issue](https://bitbucket.org/zf5/android/issues/new) and explain your issue.
Don't forget to include a logcat.